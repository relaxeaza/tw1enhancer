inject(function () {
	let timers = e.data(`${game_data.player.id}_e_timers`, {});
	let timeoutIds = {};

	let ui = (function () {
		let timer = e.bar({
			title: e.lang.timer.timer,
			icon: '_timer'
		});

		let panel = e.panel({
			title: e.lang.timer.timer,
			relative: timer.$archor,
			addClass: 'e-timer',
			width: 320
		});

		timer.click(function () {
			panel.toggle();
			return false;
		});

		panel.append(
			`<section>
				<p>${e.lang.timer.currentTime}</p>
				<p><i class="e-serverTime"></i></p>
			</section>
			<section>
				<p title="${e.lang.timer.mousewheelTip}">${e.lang.timer.wakeUpAt}: <input type="text" class="e-time"/><input type="text" class="e-date"/></p>
				<hr/>
				<p title="${e.lang.timer.descTip}"><textarea class="e-desc"></textarea></p>
				<hr/>
				<p>${e.lang.general.notifSound}: <select class="e-sounds"></select><a href="#"><span class="e-playSound e-icon play"></span></a></p>
				<hr/>
				<p><input type="button" value="${e.lang.general.create}" class="e-save"></p>
			</section>
			<section>
				<b>${e.lang.timer.activeTimers}</b>
				<br/>
				<i class="e-noneTimers">${e.lang.general.none}</i>
			</section>`
		);

		$('#serverTime').on('DOMSubtreeModified', function() {
			panel.elements.serverTime.text(dateTime());
		});

		panel.elements.time.val($('#serverTime').text())
		panel.elements.time.on('wheel', function (event) {
			let pos = event.originalEvent.wheelDelta < 0 ? 'decrease' : 'increase';
			panel.elements.time.val(changeTime(panel.elements.time.val(), pos));

			return false;
		});

		panel.elements.date.val($('#serverDate').text());
		panel.elements.date.on('wheel', function (event) {
			let pos = event.originalEvent.wheelDelta < 0 ? 'decrease' : 'increase';
			panel.elements.date.val(changeDate(panel.elements.date.val(), pos));

			return false;
		});

		panel.elements.save.click(function () {
			let dateTime = panel.elements.time.val() + ' ' + panel.elements.date.val();

			if (!dateTime.length) {
				return UI.ErrorMessage(e.lang.timer.addTime, 1500);
			}

			let status = dateStatus(dateTime);

			if (status === 0) {
				return UI.ErrorMessage(e.lang.timer.invalidDate, 1500);
			}

			if (status === 1) {
				return UI.ErrorMessage(e.lang.timer.mustBeFuture, 1500);
			}

			let id = uuid();
			let timer = {
				timestamp: status,
				dateTime: dateTime,
				desc: panel.elements.desc.val(),
				sound: panel.elements.sounds.val()
			};

			timers(id, timer);
			addTimer(id, timer, true);
		});

		for (let id in e.soundUrls) {
			panel.elements.sounds.append(`<option value="${id}">${id}</option>`);
		}

		let currentSound;
		let playing = false;

		panel.elements.sounds.change(function () {
			if (playing) {
				currentSound.pause();
				playing = false;
				panel.elements.playSound.removeClass('pause').addClass('play');
			}
		});

		panel.elements.playSound.click(function () {
			let soundId = panel.elements.sounds.val();

			if (playing) {
				currentSound.pause();
				playing = false;
				panel.elements.playSound.removeClass('pause').addClass('play');

				return false;
			}
			
			if (soundId in e.soundUrls) {
				currentSound = new Audio(e.soundUrls[soundId]);
				currentSound.play();
				playing = true;

				panel.elements.playSound.removeClass('play').addClass('pause');

				currentSound.onended = function () {
					panel.elements.playSound.removeClass('pause').addClass('play');
					playing = false;
				};
			}

			return false;
		});

		return panel;
	})();

	function addTimer (id, timer, opt_manual) {
		timeoutIds[id] = setTimeout(function () {
			let audio;

			audio = new Audio(e.soundUrls[timer.sound]);
			audio.play();

			UI.ConfirmationBox(`<p><i>${timer.dateTime}</i></p><p>${timer.desc}</p>`, [{
				text: e.lang.general.close,
				callback: function () {
					removeTimer(id);
					audio.pause();
					audio.currentTime = 0;
				}
			}], false, true, true);
		}, timer.timestamp - $.now());
		
		let $timer = ui.append(
			`<section id="e-timer-${id}" class="e-timer">
				<p class="e-header">
					<span><i>${timer.dateTime}</i></span>
					<a href="#" class="e-close"><span class="e-icon hide"></span></a>
				</p>
				<p>${timer.desc}</p>
			</section>`
		);

		if (opt_manual) {
			$timer.timer.hide();
			$timer.timer.slideDown(200);
		}

		$timer.close.click(() => removeTimer(id));

		ui.elements.noneTimers.hide();
	}

	function removeTimer (id) {
		$(`#e-timer-${id}`).remove();
		timers(true, id);

		if (Object.keys(timers()).length === 0) {
			ui.elements.noneTimers.show();
		}

		return false;
	}

	function loadQuicktimerScreens () {
		if (game_data.screen in quicktimerScreens) {
			quicktimerScreens[game_data.screen]();
			e.tooltip('.e-quickTimer');
		}

		return loadQuicktimerScreens;
	}

	function quicktimerUI (settings) {
		let $creator = $(`<a href="#" class="e-quickTimer" title="${e.lang.timer.createEventTimer}"><span class="e-icon _timer"></span></a>`);

		let duration = typeof settings.duration === 'function'
			? settings.duration()
			: settings.duration;

		$creator.click(function () {
			let endTime = new Date(serverStamp() + duration2stamp(duration));
			let id = uuid();
			let timer = {
				timestamp: endTime.getTime(),
				dateTime: endTime.toLocaleTimeString() + ' ' + endTime.toLocaleDateString(),
				desc: settings.desc
			};

			timers(id, timer);
			addTimer(id, timer);

			UI.SuccessMessage(e.lang.timer.createdTimer, 500);

			return false;
		});

		$creator.appendTo(settings.appendTo);
	}

	function duration2stamp (duration) {
		let parts = duration.split(':');
		let sec = parts[2] * 1000;
		let min = parts[1] * 1000 * 60;
		let hour = parts[0] * 1000 * 60 * 60;
		return sec + min + hour;
	}

	function dateTime () {
		let t = document.getElementById('serverTime').innerHTML;
		let d = document.getElementById('serverDate').innerHTML;

		return t + ' ' + d;
	}

	function serverStamp () {
		let t = document.getElementById('serverTime').innerHTML;
		let d = document.getElementById('serverDate').innerHTML;

		d = d.split('/');
		d = [d[1], d[0], d[2]].join('/');

		return new Date(t + ' ' + d).getTime();
	}

	function changeTime (time, position) {
		time = time.split(':');
		
		let hour = Number(time[0]);

		if (position === 'increase') {
			if (hour !== 24) {
				time[0] = ++hour;
			}

			return time.join(':');
		} else {
			if (hour > 0) {
				time[0] = --hour;
			}

			return time.join(':');
		}
	}

	function changeDate (date, position) {
		date = date.split('/');

		let day = Number(date[0]);

		if (position === 'increase') {
			if (day < 31) {
				date[0] = ++day;
			}

			return date.join('/');
		} else {
			if (day > 1) {
				date[0] = --day;
			}

			return date.join('/');
		}
	}

	function dateStatus (dateTime) {
		if (!/\d{1,2}\:\d{1,2}\:\d{1,2}\s\d{1,2}\/\d{1,2}\/\d{4}/.test(dateTime)) {
			return 0;
		}

		let split = dateTime.split(' ');
		let time = split[0];
		let date = split[1];

		date = date.split('/');
		date = date[1] + '/' + date[0] + '/' + date[2];

		let timestamp = new Date(time + ' ' + date).getTime();

		if (timestamp < new Date().getTime()) {
			return 1;
		}

		return timestamp;
	}

	function uuid () {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16);
	}

	if (!$.isEmptyObject(timers())) {
		$.each(timers(), function (id, timer) {
			addTimer(id, timer);
		});
	}

	let quicktimerScreens = {
		main: function () {
			let $build = $('#buildqueue tr[class*=buildorder]:first');

			if (!$build.length) {
				return;
			}

			quicktimerUI({
				duration: $build.find('td:eq(1)').text(),
				desc: $build.find('td:first').text() + ' | ' + `<a href="${e.url({screen:'overview'})}">${game_data.village.name} (${game_data.village.x}|${game_data.village.y})</a>`,
				appendTo: $build.find('td:eq(3)')
			});
		},
		train: function () {
			$('.trainqueue_wrap').each(function () {
				let $recruit = $('tbody:first tr.lit', this);

				quicktimerUI({
					duration: $recruit.find('td:eq(1)').text(),
					desc: $recruit.find('td:first').text().trim() + ' | ' + `<a href="${e.url({screen:'overview'})}">${game_data.village.name} (${game_data.village.x}|${game_data.village.y})</a>`,
					appendTo: $recruit.find('td:eq(2)')
				});
			});
		},
		smith: function () {
			let $research = $('#current_research tr:eq(1)');

			if (!$research.length) {
				return;
			}

			quicktimerUI({
				duration: $research.find('td:eq(1)').text(),
				desc: '${e.lang.general.research} ' + $research.find('td:first').text() + ' | ' + `<a href="${e.url({screen:'overview'})}">${game_data.village.name} (${game_data.village.x}|${game_data.village.y})</a>`,
				appendTo: $research.find('td:eq(2)')
			});
		}
	};

	$(document).on('partial_reload_end', loadQuicktimerScreens());
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}