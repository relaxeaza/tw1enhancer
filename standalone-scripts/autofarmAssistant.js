// ==UserScript==
// @name Relax Assistant TW
// @author Relaxeaza <relaxeaza.tw@gmail.com>
// @include https://*s*screen=am_farm*
// @include https://*s*screen=map*
// 31/04/2016
// ==/UserScript==

// Configurações

// intervalo entre um farm e outro em milisegundos.
// valor minimo = 201
var intervalAttacks = 500;

// página é recarregada em um intervalo de segundos.
// deixe 0 (zero) para não recarregar.
// a contagem é iniciada APÓS terminar de enviar
// os farms ou acabar as tropas.
var reloadSeconds = 10;

// quando é terminado de enviar os farms de uma aldeia
// é selecionada a próxima aldeia.
// caso ativado, anula a opção "reloadSeconds".
var villageCicle = false;

// irá farmar apenas relatorios com vitória total (verdes).
// qualquer relatório que não seja verde será ignorado
var onlyGreenReports = true;

// quando o botão A não estiver disponível, usará o
// botão B, se disponível.
var useButtonB = false;

/*
 * AutoFarmAssistant
 */
(function () {
	var intervalId;

	function init () {
		var index = 0;
		var buttonA;
		var buttonB;

		if (intervalAttacks < 201) {
			intervalAttacks = 201;
		}

		cleanBadReports();
		replaceClickFunction();

		var maxReports = jQuery('#plunder_list tr[id]').size();

		if (maxReports === 0) {
			return stop(true);
		}

		intervalId = setInterval(function () {
			buttonA = jQuery('#plunder_list tr[id]:first td:eq(8) a');
			buttonB = jQuery('#plunder_list tr[id]:first td:eq(9) a');

			if (!buttonA.hasClass('farm_icon_disabled')) {
				buttonA.click();
			} else if (useButtonB && !buttonB.hasClass('farm_icon_disabled')) {
				buttonB.click();
			}

			if (++index === maxReports) {
				return stop();
			}
		}, intervalAttacks);
	}

	function stop (noReports) {
		var dynamic = (reloadSeconds / 100) * 20;
		var random = getRandomInt(reloadSeconds - dynamic, reloadSeconds + dynamic);

		clearInterval(intervalId);

		if (noReports) {
			UI.SuccessMessage('Não existe nenhum farm para ser realizado. Recarregando...', random * 1000);

			setTimeout(function () {
				location.reload();
			}, random * 1000);

			return true;
		}

		if (villageCicle && game_data.player.villages > 1) {
			random = getRandomInt(1, 7);

			UI.SuccessMessage('Farm finalizado! Trocando de aldeia em ' + random + ' segundos...')

			setTimeout(function () {
				location.href = jQuery('#village_switch_right').attr('href');
			}, random * 1000);
		} else if (typeof reloadSeconds === 'number' && reloadSeconds) {
			UI.SuccessMessage('Farm finalizado! Recarregando em ' + random + ' segundos...')

			setTimeout(function () {
				location.reload();
			}, random * 1000);
		}
	}

	function cleanBadReports () {
		jQuery('#plunder_list tr[id]').each(function () {
			var img = jQuery('td:eq(1)', this).html();
			var disabledA = jQuery('td:eq(8) a', this).hasClass('farm_icon_disabled');
			var disabledB = jQuery('td:eq(9) a', this).hasClass('farm_icon_disabled');

			if (!useButtonB && disabledA) {
				return jQuery(this).remove();
			}

			if (disabledA && disabledB) {
				return jQuery(this).remove();
			}

			if (onlyGreenReports && img.indexOf('green.png') === -1) {
				return jQuery(this).remove();
			}

			if (img.indexOf('red.png') !== -1) {
				return jQuery(this).remove();
			}
		});
	}

	function sendUnits (target, template) {
		jQuery.post(Accountmanager.send_units_link, {
			target: target,
			template_id: template,
			source: game_data.village.id
		}, function(data) {
			if (data.error) {
				if (data.error === 'Não existem unidades suficientes') {
					stop();
				}
			} else {
				Accountmanager.farm.updateOwnUnitsAvailable(data.current_units);
			}
		}, 'json');

		return false;
	}

	function replaceClickFunction () {
		var target;
		var templates = getTemplatesId();

		jQuery('#plunder_list tr[id]').each(function () {
			target = this.id.split('_')[1];

			jQuery('td:eq(8) a', this).removeAttr('onclick').click(function () {
				return sendUnits(target, templates.a);
			});

			jQuery('td:eq(9) a', this).removeAttr('onclick').click(function () {
				return sendUnits(target, templates.b);
			});
		});
	}

	function getTemplatesId () {
		return {
			a: jQuery('form:eq(0)').attr('action').match(/template_id=(\d+)/)[1],
			b: jQuery('form:eq(1)').attr('action').match(/template_id=(\d+)/)[1]
		};
	}

	function getRandomInt (min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}

	if (game_data.screen === 'am_farm') {
		init();
	}
})();

/*
 * MapHotkeyAssistant
 */
(function () {
	var target;

	function init () {
		targetHandler();

		getTemplateIds(function (templates) {
			jQuery(document).keyup(function (event) {
				if (target) {
					var tid;

					if (event.keyCode === 65) {
						tid = templates.a;
					} else if (event.keyCode === 66) {
						tid = templates.b;
					} else {
						return false;
					}

					sendCommand(tid);
				}
			});
		});
	}

	function targetHandler () {
		jQuery('#map_mover').mousemove(function(event) {
			var pos = TWMap.map.coordByEvent(event);
			
			var x = pos[0];
			var y = pos[1];
			var village = TWMap.villages[x * 1e3 + y];

			if (village) {
				target = village.id;
			} else {
				target = false;
			}
		});
	}

	function sendCommand (tid) {
		jQuery.get(game_data.link_base_pure + 'am_farm', {
			mode: 'farm',
			ajaxaction: 'farm',
			template_id: tid,
			target: target,
			source: game_data.village.id,
			json: 1,
			h: csrf_token,
			client_time: Math.floor(new Date().getTime() / 1000)
		}, function (data) {
			if (data.error) {
				UI.ErrorMessage(data.error);
			}
		}, 'json');
	}

	function getTemplateIds (callback) {
		jQuery.get(game_data.link_base_pure + 'am_farm', function (html) {
			var a = jQuery('form:eq(0)', html).attr('action').match(/template_id=(\d+)/);
			var b = jQuery('form:eq(1)', html).attr('action').match(/template_id=(\d+)/);

			callback({
				a: a ? a[1] : false,
				b: b ? b[1] : false
			});
		});
	}

	if (game_data.screen === 'map') {
		init();
	}
})();