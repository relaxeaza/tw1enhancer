/*
 * MapHotkeyAssistant
 */
(function () {
	var target;

	function init () {
		targetHandler();

		getTemplateIds(function (templates) {
			jQuery(document).keyup(function (event) {
				if (target) {
					var tid;

					if (event.keyCode === 65) {
						tid = templates.a;
					} else if (event.keyCode === 66) {
						tid = templates.b;
					} else {
						return false;
					}

					sendCommand(tid);
				}
			});
		});
	}

	function targetHandler () {
		jQuery('#map_mover').mousemove(function(event) {
			var pos = TWMap.map.coordByEvent(event);
			
			var x = pos[0];
			var y = pos[1];
			var village = TWMap.villages[x * 1e3 + y];

			if (village) {
				target = village.id;
			} else {
				target = false;
			}
		});
	}

	function sendCommand (tid) {
		jQuery.get(game_data.link_base_pure + 'am_farm', {
			mode: 'farm',
			ajaxaction: 'farm',
			template_id: tid,
			target: target,
			source: game_data.village.id,
			json: 1,
			h: csrf_token,
			client_time: Math.floor(new Date().getTime() / 1000)
		});
	}

	function getTemplateIds (callback) {
		jQuery.get(game_data.link_base_pure + 'am_farm', function (html) {
			callback({
				a: jQuery('form:eq(0)', html).attr('action').match(/template_id=(\d+)/)[1],
				b: jQuery('form:eq(1)', html).attr('action').match(/template_id=(\d+)/)[1]
			});
		});
	}

	if (game_data.screen === 'map') {
		init();
	}
})();