inject(function () {
    e.i18n.set('pt-br', {
        "general": {
            "interval": "Intervalo",
            "min": "minutos",
            "hotkey": "Tecla de atalho",
            "no": "não",
            "never": "nunca",
            "none": "nenhum",
            "notifSound": "Som de notificação",
            "create": "criar",
            "close": "fechar",
            "research": "Pesquisa",
            "error": "Erro!",
            "success": "Sucesso!",
            "here": "aqui",
            "loading": "Carregando...",
            "points": "Pontos",
            "villages": "Aldeias"
        },
        "assistant": {
            "assistant": "Assistente",
            "updateEntries": "Atualizar lista de relatórios",
            "intervalTip": "Intervalo entre cada ativação do envio automático. Alterando esse valor, o ciclo só voltará a funcionar quando ativar manualmente o envio automático.",
            "autoSend": "Envio automático com botão",
            "isActive": "Atualmente ativado",
            "lastActive": "Última ativação",
            "sendLastReport": "Envia o primeiro relatório da lista.",
            "sendFarm": "Enviar farm com botão",
            "updated": "Lista atualizada.",
            "noMoreReports": "Não existem mais relatórios para farmar",
            "noMoreUnits": "Não existem unidades suficientes",
            "started": "Envio automático <b>iniciado<\/b>",
            "stopped": "Envio automático <b>parado<\/b>",
            "captchaAlert": "Alerta proteção de bots",
            "captchaAlertTip": "Emite um som para alertar quando a proteção contra bots aparecer, caso você não esteja presente."
        },
        "mapAssistant": {
            "assistant": "Assistente",
            "desc": "Coloque o mouse sobre uma aldeia e usa os atalhos abaixo para executar sua funcionalidade."
        },
        "timer": {
            "timer": "Alarme",
            "currentTime": "Horário atual do servidor",
            "mousewheelTip": "Use a roda do mouse para alterar os valores com mais praticidade.",
            "wakeUpAt": "Despertar às",
            "descTip": "Texto que irá aparecer ao despertar.",
            "activeTimers": "Alarmes ativos",
            "addTime": "Adicione um horário para o alarme.",
            "invalidDate": "A data inserida esta inválida. Correto: hh:mm:ss mm/dd/aaaa",
            "mustBeFuture": "O horário do alarme deve ser no futuro!",
            "createEventTimer": "Criar um alarme para este evento.",
            "createdTimer": "Alarme criado!"
        },
        "farm": {
            "selectModel": "Selecione um modelo antes!",
            "noUnitsEnough": "Não existem unidades o suficiente!",
            "desc1": "Para enviar os ataques, pressione",
            "desc2": "Na página seguinte você pode confirmar o ataque com o mesmo atalho.",
            "method": "Método de coordenadas",
            "searchAuto": "Buscar coordenadas automaticamente.",
            "searchManual": "Inserir as coordenadas manualmente.",
            "manual": "Coordenadas manuais",
            "manualSpace": "Coordenadas separadas por espaço.",
            "auto": "Coordenadas automáticas",
            "minPoints": "Pontuação mínima das aldeias",
            "maxPoints": "Pontuação máxima das aldeias",
            "radius": "Distância máxima desta aldeia",
            "playerVillages": "Aldeias de jogadores",
            "abandonedVillages": "Aldeias abandonadas",
            "playerVillagesTip": "Inclui na pesquisa aldeias que possuem dono.",
            "abandonedVillagesTip": "Inclui na pesquisa aldeias abandonadas.",
            "searchVillages": "Procurar aldeias",
            "other": "Outras opções",
            "noloopTip": "Prevenindo que ataque mais de uma vez, caso seja necessário.",
            "noloop": "Remover aldeia ao enviar ataque",
            "templates": "Modelos de tropas",
            "noTemplates": "Você não possui nenhum modelo, crie um clicando",
            "distance": "Distância",
            "clickRemove": "Clique para remover essa aldeia da lista",
            "setCoords": "Configure as coordenadas do farm!"
        },
        "filters": {
            "filters": "Filtros",
            "villageFilters": "Filtro de Aldeias",
            "desc": "Oculte aldeias no mapa usando os critérios abaixo.",
            "players": "Jogadores",
            "showPlayers": "Mostrar aldeias de jogadores",
            "playerVillageMinTip": "Pontuação mínima das aldeias com jogadores",
            "playerVillageMaxTip": "Pontuação máxima das aldeias com jogadores",
            "playerAlly": "Mostrar jogadores com tribo",
            "playerNoAlly": "Mostrar jogadores sem tribo",
            "villageMin": "Pontuação mínima",
            "villageMax": "Pontuação máxima",
            "abandoneds": "Abandonadas",
            "showAbandoneds": "Mostrar aldeias abandonadas",
            "abandonedVillageMinTip": "Pontuação mínima das aldeias abandonadas",
            "abandonedVillageMaxTip": "Pontuação máxima das aldeias abandonadas",
            "local": "Localização",
            "minRadius": "Distância mínima da aldeia atual",
            "maxRadius": "Distância máxima da aldeia atual",
            "zeroUnset": "Se você não quiser utilizar essa opção, deixe-a em branco ou zero.",
            "own": "Próprias",
            "showOwn": "Mostrar suas próprias aldeias",
            "commands": "Comandos",
            "hideAttackIncoming": "Ocultar aldeias com ataques à caminho",
            "hideAttackReturning": "Ocultar aldeias com ataques retornando",
            "hideSupportIncoming": "Ocultar aldeias com apoios à caminho",
            "reservations": "Reservas",
            "hideYourReservations": "Ocultar aldeias reservadas por você",
            "hideOtherReservations": "Ocultar aldeias reservadas pelos outros"
        },
        "profileStats": {
            "stats": "Estatísticas",
            "playerStats": "Estatísticas do Jogador",
            "daily": "Relizações diárias",
            "oda": "Unidades derrotadas atacando",
            "odd": "Unidades derrotadas defendo",
            "ods": "Unidades derrotadas apoiando",
            "loot": "Recursos saqueados",
            "vil": "Aldeias saqueadas",
            "conquer": "Aldeias conquistadas",
            "graphs": "Gráficos Tribal Wars Map"
        }
    });
});

function inject (fn) {
    var script = document.createElement('script');
    script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
    document.body.appendChild(script);
    script.remove();
}