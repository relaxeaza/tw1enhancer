inject(function () {
	let settings = e.data(`${game_data.player.id}_e_assistant`, {
		hotkeys: {
			a: 'shift+a',
			b: 'shift+b',
			c: 'shift+c',
			autoa: 'ctrl+shift+a',
			autob: 'ctrl+shift+b'
		},
		lastTimeA: 0,
		lastTimeB: 0,
		interval: 30,
		captchaAlert: 'siren'
	});

	let clickTime = 0;
	let running = false;
	let lastButton;
	let autoIntervalId;
	let autoTimeoutId;
	let autoTimeoutNotifId;

	let beep = new Audio(e.soundUrls.gentle);

	let ui = (function () {
		let assistant = e.bar({
			title: e.lang.assistant.assistant
		});

		let panel = e.panel({
			title: e.lang.assistant.assistant,
			relative: assistant.$,
			addClass: 'e-assistant',
			width: 270
		});

		assistant.click(function () {
			panel.toggle();
			return false;
		});

		panel.append(
			`<section>
				<p><input type="button" class="e-updateEntries" value="${e.lang.assistant.updateEntries}"/></p>
			</section>
			<section>
				<p title="${e.lang.assistant.intervalTip}">${e.lang.general.interval}: <input type="text" class="e-interval" value="${settings('interval')}"/> ${e.lang.general.min}</p>
				<hr/>
				<p><input type="button" title="${e.lang.general.hotkey}: ${settings('hotkeys.autoa')}" class="e-autoa" value="${e.lang.assistant.autoSend} A"/></p>
				<p><i>${e.lang.assistant.lastActive}: <span class="e-lastTimeA"></span></i></p>
				<p><input type="button" title="${e.lang.general.hotkey}: ${settings('hotkeys.autob')}" class="e-autob" value="${e.lang.assistant.autoSend} B"/></p>
				<p><i>${e.lang.assistant.lastActive}: <span class="e-lastTimeB"></span></i></p>
				<hr/>
				<p>${e.lang.assistant.isActive}: <b class="e-active">${e.lang.general.none}</b></p>
				<hr/>
				<p title="${e.lang.assistant.captchaAlertTip}">
					<span>${e.lang.assistant.captchaAlert}:</span>
					<select class="e-captchaAlert">
						<option value="none">${e.lang.general.none}</option>
					</select>
					<a href="#"><span class="e-playSound e-icon play"></span></a>
				</p>
			</section>
			<section>
				<p>${e.lang.assistant.sendLastReport}</p>
			</section>
			<section>
				<p><input type="button" title="${e.lang.general.hotkey}: ${settings('hotkeys.a')}" class="e-a" value="${e.lang.assistant.sendFarm} A"/></p>
				<p><input type="button" title="${e.lang.general.hotkey}: ${settings('hotkeys.b')}" class="e-b" value="${e.lang.assistant.sendFarm} B"/></p>
				<p><input type="button" title="${e.lang.general.hotkey}: ${settings('hotkeys.c')}" class="e-c" value="${e.lang.assistant.sendFarm} C"/></p>
			</section>`
		);

		let lastTimeA = settings('lastTimeA');
		let lastTimeB = settings('lastTimeB');

		lastTimeA = lastTimeA !== 0 ? (new Date(lastTimeA)).toLocaleString() : e.lang.general.never;
		lastTimeB = lastTimeB !== 0 ? (new Date(lastTimeB)).toLocaleString() : e.lang.general.never;

		panel.elements.lastTimeA.text(lastTimeA);
		panel.elements.lastTimeB.text(lastTimeB);
		panel.elements.updateEntries.click(updateEntries);
		panel.elements.autoa.click(() => send('a', true));
		panel.elements.autob.click(() => send('b', true));
		panel.elements.a.click(() => send('a'));
		panel.elements.b.click(() => send('b'));
		panel.elements.c.click(() => send('c'));

		panel.elements.interval.change(function () {
			clearTimeout(autoTimeoutId);
			clearTimeout(autoTimeoutNotifId);
			clearTimeout(autoIntervalId);

			if (isNaN(this.value)) {
				this.value = settings('interval');
			} else {
				settings('interval', Number(this.value));
			}
		});

		// populate sound list

		for (let id in e.soundUrls) {
			panel.elements.captchaAlert.append(`<option value="${id}">${id}</option>`);
		}

		if (settings('captchaAlert') !== 'none') {
			let elem = panel.elements.captchaAlert.find(`option[value=${settings('captchaAlert')}]`);
			elem.attr('selected', true);
		}

		panel.elements.captchaAlert.change(function () {
			settings('captchaAlert', this.value);
		});

		// preview sound

		let currentSound;
		let playing = false;

		panel.elements.captchaAlert.change(function () {
			if (playing) {
				currentSound.pause();
				playing = false;
				panel.elements.playSound.removeClass('pause').addClass('play');
			}
		});

		panel.elements.playSound.click(function () {
			let soundId = panel.elements.captchaAlert.val();

			if (playing) {
				currentSound.pause();
				playing = false;
				panel.elements.playSound.removeClass('pause').addClass('play');

				return false;
			}
			
			if (soundId in e.soundUrls) {
				currentSound = new Audio(e.soundUrls[soundId]);
				currentSound.play();
				playing = true;

				panel.elements.playSound.removeClass('play').addClass('pause');

				currentSound.onended = function () {
					panel.elements.playSound.removeClass('pause').addClass('play');
					playing = false;
				};
			}

			return false;
		});

		return panel;
	})();
	
	// 0: undefined button
	// 1: no reports
	// 2: interval protection
	// 3: button already used
	// 4: button C not exist
	// 5: insufficient units
	function send (button, initAuto) {
		if (!button) {
			return false;
		}

		if (initAuto) {
			if (running) {
				return stop();
			}

			return auto(button);
		}

		lastButton = button;

		let $report = jQuery('#plunder_list tr[id]:visible:first');

		if (!$report.size()) {
			UI.ErrorMessage(e.lang.assistant.noMoreReports, 500);
			return 1;
		}

		let $button = $report.find('.farm_icon_' + button);
		let villageId = $report.attr('id').split('_')[1];
		let time = Timing.getElapsedTimeSinceLoad();

		if (clickTime && time - clickTime < 210) {
			return 2;
		}

		clickTime = time;

		if ($button.hasClass('farm_icon_disabled')) {
			return 3;
		}

		if (button === 'c') {
			if (!$button.size()) {
				return 4;
			}

			let reportId = parseInt($report.find('td:eq(10) a').attr('onclick').split(',')[2], 10);

			TribalWars.post(Accountmanager.send_units_link_from_report, null, {
				report_id: reportId
			}, function (response) {
				Accountmanager.farm.updateOwnUnitsAvailable(response.current_units);
			});
		} else {
			let template = getTemplates()[button].id;
			
			if (!Accountmanager.farm.unitsAppearAvailableAB(template)) {
				UI.ErrorMessage(e.lang.assistant.noMoreUnits);
				return 5;
			}

			TribalWars.post(Accountmanager.send_units_link, null, {
				target: villageId,
				template_id: template,
				source: game_data.village.id
			}, function (response) {
				Accountmanager.farm.updateOwnUnitsAvailable(response.current_units);
			});
		}	
		
		$report.find('*').slideUp(120, function () {
			$report.remove();
		});

		return true;
	}

	function auto (button) {
		let upper = button.toUpperCase();
		let prop = 'lastTime' + upper;

		settings(prop, Date.now());

		ui.elements.active.text(`(${upper})`);
		ui.elements[prop].text((new Date()).toLocaleString());

		running = button;

		let intervalTime = settings('interval') * 60 * 1000;

		autoIntervalId = setInterval(function () {
			if (jQuery('#plunder_list tr[id]').size()) {
				let status = send(button);

				switch (status) {
					case 1:
					case 5:
						stop();
						updateEntries();

						autoTimeoutNotifId = setTimeout(() => beep.play(), intervalTime - 10000);
						autoTimeoutId = setTimeout(() => auto(button), intervalTime);
					break;
				}
			} else {
				stop();
				updateEntries();

				autoTimeoutNotifId = setTimeout(() => beep.play(), intervalTime - 10000);
				autoTimeoutId = setTimeout(() => auto(button), intervalTime);
			}
		}, 300);

		UI.SuccessMessage(e.lang.assistant.started);
	}

	function stop () {
		running = false;

		clearInterval(autoIntervalId);
		clearTimeout(autoTimeoutId);
		ui.elements.active.text(e.lang.general.none);
		UI.SuccessMessage(e.lang.assistant.stopped);

		return false;
	}

	function updateEntries () {
		Accountmanager.changeTroops();
	}

	function getTemplates () {
		let i = 0;
		let templates = {};

		for (let id in Accountmanager.farm.templates) {
			let x = i++ === 0 ? 'a' : 'b';
			
			templates[x] = Accountmanager.farm.templates[id];
			templates[x].id = id.split('_')[1];
		}

		return templates;
	}

	function bindHotkeys () {
		jQuery(document)
			.on('keydown', null, settings('hotkeys.a'), () => send('a'))
			.on('keydown', null, settings('hotkeys.b'), () => send('b'))
			.on('keydown', null, settings('hotkeys.c'), () => send('c'))
			.on('keydown', null, settings('hotkeys.autoa'), () => send('a', true))
			.on('keydown', null, settings('hotkeys.autob'), () => send('b', true));
	}

	function botListener () {
		console.log('botListener iniciado');

		let id;
		let visible = false;
		let notif;
		let timeLeft;
		let reinitId;

		id = setInterval(function () {
			if (document.getElementById('bot_check')) {
				console.log('proteção contra bots foi ativado');

				visible = true;

				let notifName = settings('captchaAlert');

				if (notifName !== 'none') {
					notif = new Audio(e.soundUrls[notifName]);
					notif.play();
				}

				console.log('ativação automatica parada');

				stop();
				clearInterval(id);

				reinitId = setInterval(function () {
					if (!document.getElementById('bot_check')) {
						visible = false;

						console.log('proteção contra bots foi desativada');

						clearInterval(reinitId);
						botListener();

						if (running) {
							auto(lastButton);

							console.log('ativação automatica reativada');
						} else {
							let interval = settings('interval') * 1000 * 60;

							timeLeft = interval - (Date.now() - settings('lastTime' + lastButton.toUpperCase()));

							autoTimeoutId = setTimeout(function () {
								auto(lastButton);
							}, timeLeft);

							console.log('ativação automatica reativada em ' + (timeLeft/1000) + ' segundos');
						}
					}
				}, 2000);
			}
		}, 2000);
	}

	bindHotkeys();
	botListener();
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}