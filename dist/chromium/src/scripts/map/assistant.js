inject(function () {
	if (!game_data.player.farm_manager) {
		return false;
	}

	let settings = e.data(`${game_data.player.id}_e_mapAssistant`, {
		a: 'shift+a',
		b: 'shift+b'
	});

	let raznum = /[a-z0-9]/i;
	let currentVillage = null;

	let ui = (function () {
		let assistant = e.bar({
			title: e.lang.mapAssistant.assistant
		});

		let panel = e.panel({
			title: e.lang.mapAssistant.assistant,
			relative: assistant.$,
			addClass: 'e-assistant',
			width: 270
		});

		assistant.click(function () {
			panel.toggle();
			return false;
		});

		panel.section(`<p>${e.lang.mapAssistant.desc}</p>`);
		panel.section(
			`<p>${e.lang.general.hotkey} A: <input type="button" name="a" class="e-a" value="${settings('a')}"/></p>
			<p>${e.lang.general.hotkey} B <input type="button" name="b" class="e-b" value="${settings('b')}"/></p>`
		);

		panel.elements.a.click(setHotkey);
		panel.elements.b.click(setHotkey);

		return panel;
	})();

	function setHotkey () {
		let id = this.name;
		let custom = [];

		jQuery(document)
			.off('keydown.assistant')
			.on('keydown.hotkey', function (event) {
				let char = String.fromCharCode(event.keyCode).toLowerCase();

				event.ctrlKey && !custom.includes('ctrl') && custom.push('ctrl');
				event.shiftKey && !custom.includes('shift') && custom.push('shift');
				event.altKey && !custom.includes('alt') && custom.push('alt');
				raznum.test(char) && !custom.includes(char) && custom.push(char);

				event.preventDefault();
			})
			.on('keyup.hotkey', function (event) {
				event.preventDefault();

				jQuery(document).off('keydown.hotkey keyup.hotkey');

				custom = custom.join('+');

				switch (custom) {
					case 'a':
					case 'b':
					case 'ctrl+a':
					case 'ctrl+b':
						UI.ErrorMessage(`Esse atalho é inválido.`);
						bindHotkeys();
						return false;
					break;
				}

				settings(id, custom);
				bindHotkeys();

				ui.elements[id].val(custom);
				UI.SuccessMessage('Atalho salvo!', 400);

				return false;
			});

		UI.SuccessMessage(`Pressione uma combinação de teclas para configurar o atalho...`, 100000);
	}

	function check () {
		if (!currentVillage) {
			UI.ErrorMessage('Mantenha o mouse em cima de uma aldeia.', 500);
			return false;
		}

		return true;
	}

	function assistantCommand (icon) {
		if (!currentVillage) {
			return UI.ErrorMessage('Mantenha o mouse em cima de uma abandonada.', 500);
		} else if (currentVillage.owner !== '0') {
			return UI.ErrorMessage('A aldeia precisa ser abandonada.', 500);
		}

		let url = TWMap.urls.ctx['mp_farm_' + icon]
			.replace('__village__', currentVillage.id)
			.replace('__source__', game_data.village.id);
		
		url += '&client_time=' + e.now();

		jQuery.get(url, function (data) {
			if (data.success) {
				UI.SuccessMessage(data.success);
			} else if (data.error) {
				UI.ErrorMessage(data.error);
			}
		}, 'json');
	}

	function bindVillageListener () {
		jQuery(TWMap.map.el.root).off('mousemove.assistant').on('mousemove.assistant', null, function (event) {
			let coord = TWMap.map.coordByEvent(event);
			let village = TWMap.villages[coord.join('')];
			
			currentVillage = village ? village : null;
		});
	}

	function bindHotkeys () {
		jQuery(document).off('keydown.assistant')
			.on('keydown.assistant', null, settings('a'), function () {
				assistantCommand('a');
			})
			.on('keydown.assistant', null, settings('b'), function () {
				assistantCommand('b');
			});
	}

	bindHotkeys();
	bindVillageListener();

	TWMap.map.handler.onResizeEnd = function () {
		TWMap.notifyMapSize(false);
		TWMap.isDragResizing = false;
		TWMap.popup.register();

		var e = jQuery('#map_coord_y_wrap');
		e.css('height' , e.height() - 17 + 'px');

		bindVillageListener();
	};

	TWMap.map.handler.onDragEnd = function () {
		TWMap.popup.register();
		bindVillageListener();
	};
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}