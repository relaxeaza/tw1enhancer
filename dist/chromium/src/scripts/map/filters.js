inject(function () {
	let settings = e.data(`${game_data.player.id}_e_mapFilter`, {
		player: true,
		villagePlayerMin: 0,
		villagePlayerMax: 12000,
		playerAlly: true,
		playerNoAlly: true,
		abandoned: true,
		villageAbandonedMin: 0,
		villageAbandonedMax: 12000,
		radiusMin: 0,
		radiusMax: 0,
		own: true,
		attackGoing: false,
		attackBack: false,
		supportGoing: false,
		reserved: false,
		reservedOther: false
	});

	let ui = (function () {
		let filters = e.bar({
			title: e.lang.filters.filters,
			icon: 'filter'
		});

		let panel = e.panel({
			title: e.lang.filters.villageFilters,
			relative: filters.$archor,
			addClass: 'e-filter',
			width: 260
		});

		filters.click(function () {
			panel.toggle();
			return false;
		});

		panel.append(
			`<section>
				<p>${e.lang.filters.desc}</p>
			</section>
			<section>
				<p><b>${e.lang.filters.players}</b></p>
				<p><label>${e.lang.filters.showPlayers}: <input type="checkbox" class="e-player" name="player"/></label></p>
				<div class="e-playerOptions">
					<p><label title="${e.lang.filters.playerVillageMinTip}">${e.lang.filters.villageMin}: <input type="text" class="e-villagePlayerMin" name="villagePlayerMin"/></label></p>
					<p><label title="${e.lang.filters.playerVillageMaxTip}">${e.lang.filters.villageMax}: <input type="text" class="e-villagePlayerMax" name="villagePlayerMax"/></label></p>
					<p><label>${e.lang.filters.playerAlly}: <input type="checkbox" class="e-playerAlly" name="playerAlly"/></label></p>
					<p><label>${e.lang.filters.playerNoAlly}: <input type="checkbox" class="e-playerNoAlly" name="playerNoAlly"/></label></p>
				</div>
			</section>
			<section>
				<p><b>${e.lang.filters.abandoneds}</b></p>
				<p><label>${e.lang.filters.showAbandoneds}: <input type="checkbox" class="e-abandoned" name="abandoned"/></label></p>
				<div class="e-abandonedOptions">
					<p><label>${e.lang.filters.villageMin}: <input type="text" title="${e.lang.filters.abandonedVillageMinTip}" class="e-villageAbandonedMin" name="villageAbandonedMin"/></label></p>
					<p><label>${e.lang.filters.villageMax}: <input type="text" title="${e.lang.filters.abandonedVillageMaxTip}" class="e-villageAbandonedMax" name="villageAbandonedMax"/></label></p>
				</div>
			</section>
			<section>
				<p><b>${e.lang.filters.local}</b></p>
				<p><label>${e.lang.filters.minRadius}: <input type="text" title="${e.lang.filters.zeroUnset}" class="e-radiusMin" name="radiusMin"/></label></p>
				<p><label>${e.lang.filters.maxRadius}: <input type="text" title="${e.lang.filters.zeroUnset}" class="e-radiusMax" name="radiusMax"/></label></p>
			</section>
			<section>
				<p><b>${e.lang.filters.own}</b></p>
				<p><label>${e.lang.filters.showOwn}: <input type="checkbox" class="e-own" name="own"/></label></p>
			</section>
			<section>
				<p><b>${e.lang.filters.commands}</b></p>
				<p><label>${e.lang.filters.hideAttackIncoming}: <input type="checkbox" class="e-attackGoing" name="attackGoing"/></label></p>
				<p><label>${e.lang.filters.hideAttackReturning}: <input type="checkbox" class="e-attackBack" name="attackBack"/></label></p>
				<p><label>${e.lang.filters.hideSupportIncoming}: <input type="checkbox" class="e-supportGoing" name="supportGoing"/></label></p>
			</section>
			<section>
				<p><b>${e.lang.filters.reservations}</b></p>
				<p><label>${e.lang.filters.hideYourReservations}: <input type="checkbox" class="e-reserved" name="reserved"/></label></p>
				<p><label>${e.lang.filters.hideOtherReservations}: <input type="checkbox" class="e-reservedOther" name="reservedOther"/></label></p>
			</section>`
		);

		each(panel.elements, function (id, $elem) {
			if ($elem.attr('type') === 'checkbox') {
				$elem.attr('checked', settings(id));
			} else {
				$elem.val(settings(id));
			}
		});

		if (!settings('player')) {
			panel.elements.playerOptions.hide();
		}

		if (!settings('abandoned')) {
			panel.elements.abandonedOptions.hide();
		}

		panel.$.find('input').change(function () {
			settings(this.name, this.type === 'checkbox' ? this.checked : parseInt(this.value));

			if (this.name === 'player') {
				panel.elements.playerOptions.slideToggle();
			} else if (this.name === 'abandoned') {
				panel.elements.abandonedOptions.slideToggle();
			}

			TWMap.reload();
		});

		e.tooltip(panel.$.find('[title]'));
	})();

	function filterVillage(village) {
		let points = parseInt(village.points.toString().replace(/\./g, ''), 10);
		let owner = parseInt(village.owner, 10);

		if (owner == game_data.player.id) {
			return settings('own');
		}

		if (owner) {
			if (!settings('player')) {
				return false;
			}

			if (points < settings('villagePlayerMin') || points > settings('villagePlayerMax')) {
				return false;
			}

			let ally = TWMap.players[owner].ally;

			if (ally == 0) {
				if (!settings('playerNoAlly')) {
					return false;
				}
			} else {
				if (!settings('playerAlly')) {
					return false;
				}
			}
		} else {
			if (!settings('abandoned') || points < settings('villageabandonedMin') || points > settings('villageabandonedMax')) {
				return false;
			}
		}

		if (settings('radiusMin') || settings('radiusMax')) {
			let start = village.xy.toString();
			let distance = e.coordDistance(
				start.slice(0, 3),
				start.slice(3),
				game_data.village.x,
				game_data.village.y
			);

			if (settings('radiusMin') && settings('radiusMin') > distance) {
				return false;
			}

			if (settings('radiusMax') && settings('radiusMax') < distance) {
				return false;
			}
		}

		if (village.id in TWMap.commandIcons) {
			let hide = false;

			each(TWMap.commandIcons[village.id], function (i, command) {
				if (settings('attackGoing') && command.img === 'attack') {
					hide = true;
				}

				if (settings('supportGoing') && command.img === 'incoming_support') {
					hide = true;
				}

				if (settings('attackBack') && command.img === 'return') {
					hide = true;
				}
			});

			if (hide) {
				return false;
			}
		}

		if (village.id in TWMap.reservations) {
			let by = TWMap.reservations[village.id];

			if (settings('reserved') && by === 'player') {
				return false;
			}

			if (settings('reservedOther') && by === 'team') {
				return false;
			}
		}

		return true;
	}

	TWMap.mapHandler.spawnSector = function (e, a) {
		if (!TWMap.minimap_only) {
			var i = a.x - e.x,
				t = i + TWMap.mapSubSectorSize,
				p = a.y - e.y,
				s = p + TWMap.mapSubSectorSize;

			(TWMap.church.displayed || TWMap.politicalMap.displayed || TWMap.warMode || TribalWars._settings.map_show_watchtower) && MapCanvas.createCanvas(a, e), a.dom_fragment = document.createDocumentFragment();

			var o = this._createBorder(a.x % 100 == 0);

			if (o.style.width = "1px", o.style.height = TWMap.mapSubSectorSize * TWMap.tileSize[1] + "px", a.appendElement(o, 0, 0), o = this._createBorder(a.y % 100 == 0), o.style.height = "1px", o.style.width = TWMap.mapSubSectorSize * TWMap.tileSize[0] + "px", a.appendElement(o, 0, 0), TWMap.ghost) {
				var n = TWMap.ghost.x,
					l = TWMap.ghost.y;
				if (n >= a.x && n < a.x + TWMap.mapSubSectorSize && l >= a.y && l < a.y + TWMap.mapSubSectorSize) {
					n - TWMap.ghost.x, l - TWMap.ghost.y;
					TWMap.villages[1e3 * n + l] = {
						owner: 0,
						points: 0,
						img: TWMap.ghost_village_tile,
						special: "ghost"
					}
				}
			}

			for (var r in e.tiles) {
				if (e.tiles.hasOwnProperty(r) && (r = parseInt(r), !(r < i || r >= t))) {
					for (var m in e.tiles[r]) {
						if (e.tiles[r].hasOwnProperty(m) && (m = parseInt(m), !(m < p || m >= s))) {
							var d = document.createElement("img");

							d.style.position = "absolute", d.style.zIndex = "2";

							var M = TWMap.villages[1e3 * (e.x + r) + e.y + m];

							if (M) {
								var c = M.owner,
									h = M.owner > 0 && TWMap.players[M.owner] ? TWMap.players[M.owner].ally : 0;

								var showVillage = filterVillage(M);

								if (showVillage) {
									if (0 == M.owner) {
										if (TWMap.villageColors[M.id]) {
											var W = TWMap.villageColors[M.id],
												T = TWMap.createVillageDot(W);
											a.appendElement(T, r - i, m - p)
										}
									} else {
										var W = null;

										W = M.id == game_data.village.id ?
											TWMap.colors.this :
											TWMap.getColorByPlayer(c, h, M.id), d.style.backgroundColor = "rgb(" + W[0] + "," + W[1] + "," + W[2] + ")";
									}

									imgsrc = TWMap.images[M.img],
										d.id = "map_village_" + M.id,
										d.setAttribute("src", TWMap.graphics + imgsrc),
										TribalWars._settings.map_casual_hide && parseInt(M.owner) !== parseInt(game_data.player.id) && $.inArray(M.owner, TWMap.non_attackable_players) !== -1 && (d.style.opacity = .4, d.style.filter = "alpha(opacity=40)");
									var u, y = TWMap.createVillageIcons(M);

									for (u = 0; u < y.length; u++) a.appendElement(y[u], r - i, m - p);

									$(d).mouseout(TWMap.popup.hide());
								} else {
									d.setAttribute("src", TWMap.graphics + TWMap.images[e.tiles[r][m]]);
								}
							} else {
								d.setAttribute("src", TWMap.graphics + TWMap.images[e.tiles[r][m]]);
							}

							a.appendElement(d, r - i, m - p)
						}
					}
				}
			}

			a._element_root.appendChild(a.dom_fragment), a.dom_fragment = void 0
		}
	}

	TWMap.popup.handleMouseMove = function (e) {
		var a = TWMap.map.coordByEvent(e);
		let t = a[0];
		let i = a[1];
		let n = 1e3 * t + i;
		let o = TWMap.villages[n];

		if (o && TWMap.map.inViewport(t, i) && TWMap.popup._isAwayFromContext(t, i)) {
			if (!filterVillage(o)) {
				return;
			}

			if (TWMap.context.hide(),
				"ghost" == o.special ? TWMap.map.el.root.href = TWMap.urls.ctx.mp_invite : TWMap.map.el.root.href = TWMap.urls.ctx.mp_info.replace(/__village__/, o.id),
				TWMap.popup._currentVillage = o.id,
				TWMap.map.el.mover && (TWMap.map.el.mover.style.cursor = "pointer"), !TWMap.popup.enabled)
				return !1;
			TWMap.popup._px = e.pageX,
				TWMap.popup._py = e.pageY,
				TWMap.popup._x != t || TWMap.popup._y != i ? (TWMap.popup.displayForVillage(o, t, i),
					TWMap.popup.el.fadeIn(50),
					TWMap.popup._is_visible = !0) : TWMap.popup.calcPos()
		} else {
			if (TWMap.map.el.mover) {
				var s = TWMap.warMode ? "default" : "move";
				TWMap.map.el.mover.style.cursor = s
			}
			TWMap.popup._is_visible && (TWMap.map.el.root.href = "#",
				TWMap.popup.hide())
		}
		TWMap.popup._x = t,
			TWMap.popup._y = i
	};

	TWMap.reload();
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}