inject(function () {
	let e = window.e = {};

	var soundsDir = 'chrome-extension://pmbagdgmfgidhffonllmipjihonmfdmk/sound/';

	e.soundUrls = {
		frenzy: soundsDir + 'frenzy.mp3',
		decay: soundsDir + 'decay.mp3',
		demonstrative: soundsDir + 'demonstrative.mp3',
		birdroid: soundsDir + 'birdroid.mp3',
		scratch: soundsDir + 'scratch.mp3',
		intheway: soundsDir + 'intheway.mp3',
		pop: soundsDir + 'pop.mp3',
		gentle: soundsDir + 'gentle.mp3'
	};

	e.unitsOrder = (function () {
		let units = ['spear', 'sword', 'axe', 'spy', 'light', 'heavy', 'ram', 'catapult', 'knight', 'snob', 'militia'];

		if ('archer' in UnitPopup.unit_data) {
			units.splice(3, 0, 'archer');
			units.splice(6, 0, 'marcher');
		}

		return units;
	})();

	e.i18n = (function () {
		let _default = 'pt-br';

		e.locales = {};
		e.lang = null;

		return {
			set: function (locale, data) {
				e.locales[locale] = data;

				if (locale === _default) {
					e.i18n.select(locale);
				}
			},
			select: function (locale) {
				if (locale in e.locales) {
					e.lang = e.locales[locale];
				} else {
					throw new Error(`The language ${locale} is not set.`);
				}
			}
		};
	})();

	e.position = function ($elem, $relative) {
		let relativeWidth = $relative.width();
		let relativeHeight = $relative.height();
		let offset = $relative.offset();

		offset.left += (relativeWidth - $elem.width()) / 2;
		offset.top += relativeHeight + 3;

		let panelWidth = $elem.width();
		let docWidth = jQuery(window).width();

		if (offset.left < 0) {
			offset.left = 3;
		}

		if (offset.top < 0) {
			offset.top = 3;
		}

		if (docWidth - (offset.left + panelWidth) < 0) {
			offset.left = 'auto';
			offset.right = 3;
		}

		return offset;
	}

	e.panel = (function () {
		jQuery(document).click(function (event) { 
			if (jQuery(event.target).closest('.autoHideBox').length) {
				return;
			}

			if (jQuery(event.target).closest('.e-panel').length) {
				return;
			}

			jQuery('div.e-panel:visible').fadeOut(100);
		});

		return function (settings) {
			settings = jQuery.extend({
				width: 200
			}, settings);

			let panel = {
				elements: {},
				all: {}
			};

			let $relative = jQuery(settings.relative);

			panel.$ = jQuery('<div class="e e-panel" style="visibility:hidden"></div>').appendTo('body');

			panel.$header = jQuery('<header></header>')
				.appendTo(panel.$);

			panel.$.draggable({
				handle: 'header'
			});

			panel.$hide = jQuery('<span class="e-icon hide"></span>')
				.click(function () {
					panel.hide();

					return false;
				})
				.appendTo(panel.$header);

			if ('title' in settings) {
				panel.$title = jQuery(`<span class="e-title"></span>`).append(settings.title);
				panel.$header.append(panel.$title);
			}

			if ('content' in settings) {
				if (Array.isArray(settings.content)) {
					jQuery.each(settings.content, function (i, item) {
						panel.$.append(`<section><div>${item}</div></section>`);
					});
				} else {
					panel.$.append(settings.content);
				}
			}

			if ('addClass' in settings) {
				panel.$.addClass(settings.addClass);
			}

			let offset = e.position(panel.$, $relative);

			panel.$.css({
				width: settings.width
			});

			panel.$.css(offset);

			panel.show = function () {
				jQuery('.e-panel').fadeOut(100);

				if (settings.reposition) {
					let offset = e.position(panel.$, $relative);
					panel.$.css(offset);
				}
				
				panel.$.fadeIn(100);

				return panel;
			};

			panel.hide = function () {
				panel.$.fadeOut(100);

				return panel;
			};

			panel.toggle = function () {
				panel[panel.$.is(':visible') ? 'hide' : 'show']();

				return panel;
			};

			panel.move = function (x, y, ms) {
				panel.$.animate({
					top: x,
					left: y
				}, ms);

				return panel;
			};

			panel.destroy = function () {
				delete panel;
			};

			panel.section = function (content) {
				return panel.append(`<section>${content}</section>`);
			};

			panel.append = function (content) {
				let $content = jQuery(content);

				panel.$.append($content);

				let elements = {};
				
				$content.find('[class^=e-]').add($content.filter('[class^=e-]')).each(function () {
					let name = this.className.match(/e\-([^ ]+)/)[1];

					elements[name] = panel.elements[name] = jQuery(this);
				});

				e.tooltip(panel.$.find('[title]'));

				return elements;
			};

			panel.$.hide();
			panel.$.css('visibility', '');

			return panel;
		}
	})();

	e.bar = function (options) {
		let bar = {};

		bar.$ = jQuery(
			`<td class="topAlign">
				<table class="header-border">
					<tbody>
						<tr><td><table class="box menu nowrap"><tbody><tr><td class="firstcell"><div class="e e-bar"></div></td></tr></tbody></table></td></tr>
						<tr class="newStyleOnly"><td class="shadow"><div class="leftshadow"> </div><div class="rightshadow"> </div></td></tr>
					</tbody>
				</table>
			</td>`
		).hide();

		bar.$.insertAfter('#header_info td.topAlign:not([align], [id]):last');

		let $bar = bar.$.find('.e-bar');

		bar.append = function (content) {
			bar.show();
			return jQuery('<div>').append(content).appendTo($bar);
		};

		bar.hide = function () {
			bar.$.hide();
			return bar;
		};

		bar.show = function () {
			bar.$.fadeIn(500);
			return bar;
		};

		bar.clean = function () {
			$bar.empty();
			return bar;
		};

		bar.click = function (callback) {
			$bar.click(callback);
			return bar;
		};

		bar.$archor = bar.append(`<a href="#">${options.title}</a>`);

		if (options.icon) {
			bar.$archor.prepend(`<span class="e-icon ${options.icon}"></span> `);
		}

		if (options.click) {
			bar.$archor.click(options.click);
		}

		bar.show();

		return bar;
	};

	e.url = function (params, isPost) {
		params = jQuery.extend({
			screen: game_data.screen,
			village: game_data.village.id
		}, params);

		if (isPost) {
			params.h = game_data.csrf;
		}

		if (game_data.player.sitter !== '0') {
			params.t = game_data.player.sitter;
		}

		return '/game.php?' + jQuery.param(params);
	};

	e.hasCoords = function (string) {
		if (!string || typeof string !== 'string') {
			return false;
		}

		return /\d{1,3}\|\d{1,3}/.test(string);
	};

	e.proxyLoad = function (options) {
		options.type = options.type || 'json';
		options.proxy = options.proxy || 'jsonproxy';

		let parsedUrl;
		let query;

		if (options.proxy === 'jsonproxy') {
			options.url = encodeURIComponent(options.url);
			parsedUrl = `https://jsonp.afeld.me/?url=${options.url}`;
		} else if (options.proxy === 'yql') {
			query = encodeURIComponent(`select * from html where url="${options.url}"`);
			parsedUrl = `https://query.yahooapis.com/v1/public/yql?q=${query}&format=${options.type}`;
		}

		return jQuery.ajax({
			type: 'get',
			url: parsedUrl,
			dataType: options.type,
			timeout: 5000,
			success: options.success,
			error: options.error,
			complete: options.complete
		});
	};

	e.tooltip = (function () {
		let $tooltip = jQuery('#e-tooltip');
		let $win = jQuery(window);

		if (!$tooltip.size()) {
			$tooltip = jQuery('<div id="e-tooltip" class="e"></div>').appendTo('body');
		}

		function tooltip (expr) {
			let visible = false;

			jQuery(expr)
			.each(function () {
				let title = this.getAttribute('title');

				jQuery(this).data('e-tooltip', title);
				this.removeAttribute('title');
			})
			.off('mouseover.tooltip mouseout.tooltip mousemove.tooltip')
			.on({
				'mouseover.tooltip': function () {
					$tooltip.html(jQuery(this).data('e-tooltip'));
					$tooltip.show();
					visible = true;
				},
				'mouseout.tooltip': function () {
					$tooltip.hide();
					visible = false;
				},
				'mousemove.tooltip': function () {
					let width = $tooltip.width();
					let winWidth = $win.width();

					if (event.pageX + width > winWidth) {
						$tooltip.css({
							left: event.pageX - 15 - width,
							top: event.pageY - 5
						});
					} else {
						$tooltip.css({
							left: event.pageX + 15,
							top: event.pageY - 5
						});
					}
				}
			});

			return {
				visible: function () {
					return visible;
				}
			};
		}

		tooltip.html = function (html) {
			jQuery('#e-tooltip').html(html);
		};

		tooltip.hide = function (html) {
			jQuery('#e-tooltip').hide();
		};

		tooltip.show = function (html) {
			jQuery('#e-tooltip').show();
		};

		return tooltip;
	})();

	e.coordDistance = function () {
		let start;
		let dest;
		let args = jQuery.makeArray(arguments);

		if (args.length === 2) {
			start = args[0].split('|');
			dest = args[1].split('|');
		} else if (args.length === 4) {
			start = args.slice(0, 2);
			dest = args.slice(2, 4);
		}

		let distance = Math.sqrt(Math.pow(start[0] - dest[0], 2) + Math.pow(start[1] - dest[1], 2));

		return Math.round(distance * 100) / 100;
	};

	e.renameVillage = function (villageId, name, callback) {
		jQuery.post(`/game.php?village=${villageId}&screen=main&action=change_name&h=${game_data.csrf}`, {
			name: name
		}, function (html) {
			callback(jQuery('.error_box', html).text());
		});
	};

	e.data = function (id, defaults) {
		let data = localStorage.getItem(id);

		if (data) {
			data = JSON.parse(data);
		} else {
			data = defaults;
			localStorage.setItem(id, JSON.stringify(data));
		}

		return function (remove, key, value) {
			if (remove === undefined) {
				return data;
			}

			if (remove !== true) {
				value = key;
				key = remove;
				remove = false;
			}

			let route = data;
			let keys = key.split('.');
			let lastKey = keys[keys.length - 1];

			keys.forEach(function (key, index) {
				if (index + 1 !== keys.length && /object|array/.test(jQuery.type(route[key]))) {
					route = route[key];
				}
			});

			if (remove) {
				delete route[lastKey];
				localStorage.setItem(id, JSON.stringify(data));
			} else if (value === undefined) {
				return route[lastKey];
			} else {
				route[lastKey] = value;
				localStorage.setItem(id, JSON.stringify(data));
			}

			return true;
		};
	};

	e.now = function () {
		return Math.round(Date.now() / 1000);
	};

	e.getNum = function (string) {
		return parseInt(string.match(/\d+/)[0], 10);
	};

	e.mapSection = function (x, y, callback) {
		let data = {
			v: 2,
			e: Date.now()
		};

		x -= x % 20; // 520
		y -= y % 20; // 520

		x3 = x;		 // 520
		y3 = y - 20; // 500

		x4 = x;		 // 520
		y4 = y + 20; // 540

		x1 = x - 20; // 500
		y1 = y; 	 // 520

		x2 = x + 20; // 540
		y2 = y; 	 // 520

		x5 = x + 20; // 540
		y5 = y + 20; // 540

		x6 = x - 20; // 500
		y6 = y - 20; // 500

		data[`${x}_${y}`] = 0;
		data[`${x1}_${y1}`] = 0;
		data[`${x2}_${y2}`] = 0;
		data[`${x3}_${y3}`] = 0;
		data[`${x4}_${y4}`] = 0;
		data[`${x5}_${y5}`] = 0;
		data[`${x6}_${y6}`] = 0;

		jQuery.get('/map.php', data, function (sectors) {
			let villages = {};
			let players = {};
			let allies = {};

			for (let sectorIndex = 0; sectorIndex < sectors.length; sectorIndex++) {
				let sector = sectors[sectorIndex];

				let sectorX = sector.x;
				let sectorY = sector.y;

				let sectorVillages = sector.data.villages;
				let sectorPlayers = sector.data.players;
				let sectorAllies = sector.data.allies;

				for (let villageX = 0; villageX < sectorVillages.length; villageX++) {
					for (let villageY in sectorVillages[villageX]) {
						let village = sectorVillages[villageX][villageY];

						let [id, img, name, points, owner, mood, bonus, event_special] = sectorVillages[villageX][villageY];

						let x = sectorX + villageX;
						let y = sectorY + parseInt(villageY, 10);

						villages[id] = { id, name, points, owner, x, y };

						villages[id].points = parseInt(villages[id].points.replace(/\./g, ''), 10)
					}
				}

				Object.assign(players, sectorPlayers);
				Object.assign(allies, sectorAllies);
			}

			callback(villages, players, allies);
		}, 'json');
	};

	e.randomMinMax = function (min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	each = jQuery.each;
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}