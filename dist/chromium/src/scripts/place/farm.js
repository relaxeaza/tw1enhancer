inject(function () {
	// if (game_data.mode !== null) {
	// 	if (game_data.mode !== 'command') {
	// 		return false;
	// 	}
	// }

	let settings = e.data(`${game_data.player.id}_e_farm_${game_data.village.id}`, {
		method: 'auto',
		index: 0,
		auto: {
			villages: [],
			minpoints: 0,
			maxpoints: 1000,
			radius: 10,
			players: false,
			abandoneds: true
		},
		manual: {
			coords: []
		},
		template: 0,
		noloop: false,
		hotkey: 'shift+q'
	});

	let running = false;
	let oldVillages = settings('auto.villages');

	function sendFarm () {
		running = true;

		let villages = settings('method') === 'auto'
			? settings('auto.villages')
			: settings('manual.coords');

		if (settings('index') >= villages.length || settings('noloop')) {
			settings('index', 0);
		}

		if (!location.search.includes('try=confirm')) {
			if (settings('template') === 0) {
				return UI.ErrorMessage(e.lang.farm.selectModel);
			}

			let units = TroopTemplates.current[settings('template')];
			let error = false;

			each(units, function (unit, amount) {
				let $input = $('#unit_input_' + unit);

				if (!$input.size() || amount == 0) {
					return;
				}

				if ($input.data('all-count') < amount) {
					UI.ErrorMessage(e.lang.farm.noUnitsEnough);
					error = true;
					running = false;

					return false;
				}

				$input.val(amount);
			});

			if (!error) {
				let coords = settings('method') === 'auto'
					? [villages[settings('index')].x, villages[settings('index')].y].join('|')
					: villages[settings('index')];

				$('#place_target > input').val(coords);
				$('#target_attack').click();
			}
		} else {
			if (settings('noloop')) {
				let villages;

				villages = settings(settings('method'));
				villages.splice(0, 1);

				settings(settings('method'), villages)
			} else {
				settings('index', settings('index') + 1);
			}

			$('#troop_confirm_go').click();
		}
	}

	function currentState () {
		if (document.querySelector('#command-data-form')) {
			return 'prepare';
		}

		if (document.querySelector('#troop_template')) {
			return 'template';
		}

		return 'confirm';
	}

	function populateAutoVillages () {
		if (!settings('auto.villages').length) {
			return false;
		}

		if (currentState() === 'confirm') {
			return false;
		}

		ui.elements.villages.empty();

		each(settings('auto.villages'), function (i, village) {
			let url = e.url({ screen: 'info_village', id: village.id });

			if (village.name == 0) {
				village.name = 'Aldeia de bárbaros';
			}

			let $li = $(`<li title="${e.lang.farm.distance}: ${village.distance} | ${e.lang.farm.clickRemove}"><a href="${url}">${village.name} (${village.x}|${village.y})</a></li>`);

			$li.click(function (event) {
				if (event.which === 2 || event.ctrlKey) {
					return;
				}

				settings(true, `auto.villages.${village.id}`);
				$li.remove();

				return false;
			});

			$li.appendTo(ui.elements.villages);
			e.tooltip($li);
		});
	}

	function searchVillages () {
		ui.elements.search.val(e.lang.general.loading);

		e.mapSection(game_data.village.x, game_data.village.y, function (objVillages) {
			let villages = [];

			for (let id in objVillages) {
				let village = objVillages[id];

				village.distance = e.coordDistance(game_data.village.x, game_data.village.y, village.x, village.y);
				villages.push(village);
			}

			villages = villages.filter(function (village) {
				let distance = e.coordDistance(game_data.village.x, game_data.village.y, village.x, village.y);

				if (distance > settings('auto.radius')
					|| village.points > settings('auto.maxpoints')
					|| village.points < settings('auto.minpoints')
					|| !settings('auto.abandoneds') && village.owner == 0
					|| !settings('auto.players') && village.owner != 0
					|| village.owner == game_data.player.id
				) {
					return false;
				}

				return true;
			});

			villages = villages.sort(function (a, b) {
				return a.distance - b.distance;
			});

			settings('auto.villages', villages);

			populateAutoVillages();
			ui.elements.search.val(e.lang.farm.searchVillages);
		});
	}

	let ui = (function () {
		if (currentState() === 'confirm') {
			return false;
		}

		let panel;

		let farm = e.bar({
			title: 'Farm',
			click: function () {
				panel.toggle();
				return false;
			}
		});

		panel = e.panel({
			title: 'Farm',
			relative: farm.$archor,
			addClass: 'e-farm',
			width: 270
		});

		panel.append(
			`<section>
				<p>${e.lang.farm.desc1} <b>${settings('hotkey')}</b>. ${e.lang.farm.desc2}</p>
			</section>
			<section class="e-methods">
				<p><b>${e.lang.farm.method}</b></p>
				<p><label><input type="radio" name="e-coords-method" class="e-method-auto" value="auto"/> ${e.lang.farm.searchAuto}</label></p>
				<p><label><input type="radio" name="e-coords-method" class="e-method-manual" value="manual"/> ${e.lang.farm.searchManual}</label></p>
			</section>
			<section class="e-manual">
				<p><b>${e.lang.farm.manual}</b></p>
				<p><textarea class="e-manualTextarea"></textarea></p>
				<p><i>${e.lang.farm.manualSpace}</i></p>
			</section>
			<section class="e-auto">
				<p><b>${e.lang.farm.auto}</b></p>
				<p><label>${e.lang.farm.minPoints}: <input type="text" class="e-minpoints" name="minpoints"/></label></p>
				<p><label>${e.lang.farm.maxPoints}: <input type="text" class="e-maxpoints" name="maxpoints"/></label></p>
				<p><label>${e.lang.farm.radius}: <input type="text" class="e-radius" name="radius"/></label></p>
				<p><label title="${e.lang.farm.playerVillagesTip}">${e.lang.farm.playerVillages}: <input type="checkbox" class="e-players" name="players"/></label></p>
				<p><label title="${e.lang.farm.abandonedVillagesTip}">${e.lang.farm.abandonedVillages}: <input type="checkbox" class="e-abandoneds" name="abandoneds"/></label></p>
				<p><input type="button" class="e-search" value="${e.lang.farm.searchVillages}"/></p>
				<ul class="e-villages"></ul>
			</section>
			<section>
				<p><b>${e.lang.farm.other}</b></p>
				<p title="${e.lang.farm.noloopTip}">
					<label>${e.lang.farm.noloop}: <input type="checkbox" class="e-noloop" name="noloop"/></label>
				</p>
			</section>
			<section class="e-templateSection">
				<p><b>${e.lang.farm.templates}</b></p>
				<p class="e-notemplates">${e.lang.farm.noTemplates} <b><a class="e-templatesUrl">${e.lang.general.here}</a></b>.</p>
				<ul class="e-templates">
					<li><label><input type="radio" name="e-template" value="0"> ${e.lang.general.none}</label></li>
				</ul>
			</section>`
		);

		panel.elements.templatesUrl.click(function () {
			location.href = e.url({screen: 'place', mode: 'templates'});
		});

		panel.elements.manualTextarea.text(settings('manual.coords').join(' '));
		panel.elements['method-' + settings('method')].attr('checked', true);

		panel.elements.methods.find('input').change(function () {
			settings('method', this.value);
			panel.elements.manual.slideToggle();
			panel.elements.auto.slideToggle();
		});

		panel.elements.minpoints.val(settings('auto.minpoints'));
		panel.elements.maxpoints.val(settings('auto.maxpoints'));
		panel.elements.radius.val(settings('auto.radius'));
		panel.elements.players.attr('checked', !!settings('auto.players'));
		panel.elements.abandoneds.attr('checked', !!settings('auto.abandoneds'));

		panel.elements.auto.find('input').change(function () {
			settings(`auto.${this.name}`, this.type === 'checkbox' ? this.checked : this.value);
		});

		panel.elements.search.click(searchVillages);
		panel.elements.noloop.attr('checked', !!settings('noloop'));

		panel.elements.noloop.change(function () {
			settings('noloop', this.checked);
		});

		panel.elements[Array.isArray(TroopTemplates.current) ? 'templates' : 'notemplates'].hide();
		panel.elements[settings('method') === 'auto' ? 'manual' : 'auto'].hide();

		if (!$.isEmptyObject(TroopTemplates.current)) {
			each(TroopTemplates.current, function (id, template) {
				panel.elements.templates.append(`<li><label><input type="radio" name="e-template" value="${id}"> ${template.name}</label></li>`);
			});

			panel.elements.templates.find('input').change(function () {
				settings('template', this.value);
			});
		}

		panel.elements.templates.find('[value=' + settings('template') + ']').attr('checked', true);

		return panel;
	})();

	$(document).on('keydown', null, settings('hotkey'), function () {
		if (!settings(settings('method')).villages.length) {
			return UI.InfoMessage(e.lang.farm.setCoords);
		}

		if (running) {
			return false;
		}

		sendFarm();
	});

	populateAutoVillages();
});

function inject (fn) {
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('!' + fn.toString() + '();'));
	document.body.appendChild(script);
	script.remove();
}